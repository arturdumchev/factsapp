package com.livermor.hatethisfact.models;

/**
 * Created by arturdumchev on 25.09.15.
 */
public class Fact {
    private String mFactText;
    private int mFactReputation;
    private boolean isHated;

    public Fact(String s) {
        mFactText = s;
    }

    public String getFactText() {
        return mFactText;
    }

    public void setFactText(String factText) {
        mFactText = factText;
    }

    public void setFactReputation(int factReputation) {
        mFactReputation = factReputation;
    }

    public boolean isHated() {
        return isHated;
    }

    public void setIsHated(boolean isHated) {
        this.isHated = isHated;
    }
}
