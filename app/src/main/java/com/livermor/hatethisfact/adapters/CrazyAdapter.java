package com.livermor.hatethisfact.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jpardogo.android.flabbylistview.lib.FlabbyLayout;
import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.managers.ColorManager;
import com.livermor.hatethisfact.managers.ShareManager;
import com.livermor.hatethisfact.ui.puppeteers.MainFragment;
import com.livermor.hatethisfact.ui.puppeteers.MainActivity;
import com.livermor.hatethisfact.ui.interfaces.Top37Hated;
import java.util.List;

public class CrazyAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private List<String> mItems;
    private List<String> mItems2;
    private FragmentTransaction ft;

    private static boolean linkToManeFragmentWasShown = false;

    public CrazyAdapter(Context context, FragmentTransaction ft, List<String> items, List<String> items2) {
        super(context, 0, items);
        mContext = context;
        mItems = items;
        mItems2 = items2;

        this.ft = ft;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public String getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        //attach different layout item on the end
        if (position == Top37Hated.TOP_HATED_AMOUNT) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.top_hated_last_item, parent, false);

            ImageButton button = (ImageButton) convertView.findViewById(R.id.hate_more_facts_button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //change item in drawer
                    MainActivity.getNavigationView().getMenu().getItem(0).setChecked(true);

                    Fragment mFragment = new MainFragment();
                    ft.replace(R.id.container, mFragment, MainActivity.VISIBLE_FRAGMENT);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

            linkToManeFragmentWasShown = true;
            return convertView;
        }

        if (convertView == null || linkToManeFragmentWasShown) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.top_hated_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position));
        holder.text2.setText(mItems2.get(position));

        holder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareManager.showDialog(
                        mContext.getString(R.string.share_title_basic),
                        String.valueOf(holder.text.getText()));
            }
        });


        int color = ColorManager.getColor();
        ((FlabbyLayout) convertView).setFlabbyColor(color);
        if (ColorManager.isDarkColor(color)) {
            holder.text.setTextColor(ContextCompat.getColor(mContext, R.color.text_for_dark));
            holder.text2.setTextColor(ContextCompat.getColor(mContext, R.color.text_for_dark));
            holder.shareButton.setImageResource(R.drawable.ic_action_share_whight);
        } else {
            holder.text.setTextColor(ContextCompat.getColor(mContext, R.color.text_for_light));
            holder.text2.setTextColor(ContextCompat.getColor(mContext, R.color.text_for_light));
            holder.shareButton.setImageResource(R.drawable.ic_action_share);
        }

        return convertView;
    }

    static class ViewHolder {

        TextView text;
        TextView text2;
        ImageButton shareButton;

        public ViewHolder(View view) {
            text = (TextView) view.findViewById(R.id.text_in_crazy);
            text2 = (TextView) view.findViewById(R.id.amount_of_haters);
            shareButton = (ImageButton) view.findViewById(R.id.share_top37);
        }
    }


}
