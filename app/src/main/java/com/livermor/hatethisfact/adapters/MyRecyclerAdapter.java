package com.livermor.hatethisfact.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.data.BackendHelper;
import com.livermor.hatethisfact.data.DatabaseSQLiteSingleton;
import com.livermor.hatethisfact.data.ExternalDataBaseHelper;
import com.livermor.hatethisfact.data.MySharedPrefs;
import com.livermor.hatethisfact.models.Fact;
import com.livermor.hatethisfact.managers.ShareManager;
import com.livermor.hatethisfact.managers.SoundManager;

import java.util.Collections;
import java.util.List;



public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    List<Fact> data = Collections.emptyList();
    SharedPreferences prefs;

    public MyRecyclerAdapter(Context context, List<Fact> data) {
        prefs = MySharedPrefs.getmPrefs();
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.my_hated_list_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Fact current = data.get(position);
        holder.factText.setText(current.getFactText());
        holder.isHatedCheckBox.setChecked(current.isHated());
        holder.isHatedCheckBox.setTag(current);

        holder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareManager.showDialog(
                        inflater.getContext().getString(R.string.share_title_basic),
                        current.getFactText()
                );
            }
        });

        holder.isHatedCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SoundManager.playSound(R.raw.hate_sound);

                CheckBox chB = (CheckBox) v;
                Fact temp = (Fact) chB.getTag();

                String fact = temp.getFactText();
                boolean b = chB.isChecked();

                changeHateToGoodAttribute(fact, b);
            }
        });
    }

    public static void changeHateToGoodAttribute(String fact, Boolean b) {
        SQLiteDatabase db = DatabaseSQLiteSingleton.getDb();

        BackendHelper.saveHatred(fact, !b);

        ContentValues factValues = new ContentValues();
        factValues.put(ExternalDataBaseHelper.IS_HATED, !b);
        db.update(ExternalDataBaseHelper.TABLE_NAME,
                factValues,
                "FACT = ?", // where Fact == current fact
                new String[]{fact});
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView factText;
        CheckBox isHatedCheckBox;
        ImageButton shareButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            factText = (TextView) itemView.findViewById(R.id.fact_text);
            isHatedCheckBox = (CheckBox) itemView.findViewById(R.id.is_hated_checkbox);
            shareButton = (ImageButton) itemView.findViewById(R.id.share_my_hated);
        }
    }
}
