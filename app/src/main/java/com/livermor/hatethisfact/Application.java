package com.livermor.hatethisfact;

import android.content.res.Configuration;

import com.facebook.FacebookSdk;
import com.livermor.hatethisfact.data.BackendHelper;
import com.livermor.hatethisfact.managers.PopupManager;


/**
 * Created by arturdumchev on 22.09.15.
 */
public class Application extends android.app.Application {


    private static final String TAG = Application.class.getSimpleName() + ": ";
    private static Application instance;
    private static boolean firstStart = true;


    public static Application get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        BackendHelper.initialization(this);//parse.com backend
        FacebookSdk.sdkInitialize(getApplicationContext());
        instance = this;//giving context to Singletons
        BackendHelper.updateDatabase(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    public static boolean isFirstStart() {
        return firstStart;
    }

    public static void setFirstStart(boolean f) {
         firstStart = f;
    }
}
