package com.livermor.hatethisfact.ui.interfaces;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;

import android.os.Bundle;

import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livermor.hatethisfact.Application;
import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.adapters.MyRecyclerAdapter;
import com.livermor.hatethisfact.data.BackendHelper;
import com.livermor.hatethisfact.managers.ColorManager;
import com.livermor.hatethisfact.data.DatabaseSQLiteSingleton;

import com.livermor.hatethisfact.models.Fact;
import com.livermor.hatethisfact.ui.puppeteers.MainActivity;
import com.livermor.hatethisfact.ui.puppeteers.MainFragment;

import java.util.ArrayList;
import java.util.List;


public class MyHatedList extends Fragment {

    private final static String TAG = MyHatedList.class.getSimpleName() + " REPORTS (!)";

    DatabaseSQLiteSingleton db;

    RecyclerView mRecyclerView;
    MyRecyclerAdapter mAdapter;

    SharedPreferences prefs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.my_hated_list_fragment, container, false);

        layout.setBackgroundColor(ColorManager.getBackgroundColor());

        Cursor cursor = DatabaseSQLiteSingleton.getCursorForMyHatedList();

        if (cursor.getCount() == 0) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            showDialog(ft);
            BackendHelper.getFactsFromServer(70);
        }

        mRecyclerView = (RecyclerView) layout.findViewById(R.id.my_hated_list_recycler);
        mAdapter = new MyRecyclerAdapter(getActivity(), getData(cursor));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return layout;
    }


    public static List<Fact> getData(Cursor cursor) {
        List<Fact> data = new ArrayList<>();

        String temp = new String();

        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            String fact = cursor.getString(1);
            Fact current = new Fact(fact);
            data.add(current);

            temp += fact + " \n";
        }
        DatabaseSQLiteSingleton.setFactToShare(temp);//crutch for sharing

        return data;
    }


    @Override
    public void onPause() {
        super.onPause();
        db.closeCursorForMyHatedList();
    }


    public void showDialog(final FragmentTransaction ft) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity(), R.style.my_alert_dialog_style);
        builder.setTitle("You don't have hated facts yet");
        builder.setPositiveButton("Find them!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //change item in drawer
                MainActivity.getNavigationView().getMenu().getItem(0).setChecked(true);

                Fragment current = getActivity()
                        .getSupportFragmentManager()
                        .findFragmentByTag(MainActivity.VISIBLE_FRAGMENT);

                //change current fragment
                ft.detach(current);
                ft.replace(R.id.container, new MainFragment(), MainActivity.VISIBLE_FRAGMENT);
                ft.commit();
            }
        });

        builder.show();
    }


}
