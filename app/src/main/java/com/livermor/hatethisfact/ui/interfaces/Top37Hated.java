package com.livermor.hatethisfact.ui.interfaces;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.livermor.hatethisfact.R;

import com.livermor.hatethisfact.adapters.CrazyAdapter;
import com.livermor.hatethisfact.data.DatabaseSQLiteSingleton;
import com.livermor.hatethisfact.managers.PopupManager;
import com.livermor.hatethisfact.managers.SoundManager;


import java.util.ArrayList;
import java.util.List;

public class Top37Hated extends ListFragment {
    /*
    * ***********************************************************************************************************/
    public static final int TOP_HATED_AMOUNT = 37;
    private CrazyAdapter mAdapter;

    private final static String TAG = Top37Hated.class.getSimpleName() + "REPORTS(!)";

    SharedPreferences prefs;

    List<String> items;
    List<String> items2;
    /*
    * ***********************************************************************************************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.top_fragment_hated, container, false);

        populateListItems();

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        mAdapter = new CrazyAdapter(getActivity(), ft, items, items2);
        setListAdapter(mAdapter);

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    SoundManager.playSound(R.raw.whiff);
                }
                return false;
            }
        });

        return layout;
    }
    /*
    * ***********************************************************************************************************/

    private void populateListItems() {

        items = new ArrayList<String>(TOP_HATED_AMOUNT);//from 0 to TOP_HATED_AMOUNT + last link to main fragment
        items2 = new ArrayList<String>(TOP_HATED_AMOUNT);

        for (int i = 0; i <= TOP_HATED_AMOUNT; i++) {//just a crutch
            DatabaseSQLiteSingleton.getCursorForTOP37Hated().moveToPosition(i);
            String temp = DatabaseSQLiteSingleton.getCursorForTOP37Hated().getString(1); //getting fact text
            items.add(temp);

            String temp2 = "\n\n * " + DatabaseSQLiteSingleton.getCursorForTOP37Hated().getInt(2) + " guys hate this";//get fact reputation
            items2.add(temp2);
        }

        DatabaseSQLiteSingleton.setFactToShare("See the TOP hated fact with \"Hate this fact\" App!");
    }

    @Override
    public void onPause() {
        super.onPause();
        DatabaseSQLiteSingleton.closeCursorForTOP37Hated();
    }

    @Override
    public void onResume() {
        super.onResume();

        PopupManager.firstStart37();

    }
}
