package com.livermor.hatethisfact.ui.puppeteers;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.res.Configuration;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;

import com.facebook.CallbackManager;
import com.livermor.hatethisfact.Application;
import com.livermor.hatethisfact.R;

import com.livermor.hatethisfact.data.BackendHelper;
import com.livermor.hatethisfact.data.DatabaseSQLiteSingleton;
import com.livermor.hatethisfact.data.MySharedPrefs;
import com.livermor.hatethisfact.managers.ColorManager;
import com.livermor.hatethisfact.managers.NotificationMan;
import com.livermor.hatethisfact.managers.PopupManager;
import com.livermor.hatethisfact.managers.ShareManager;
import com.livermor.hatethisfact.managers.SoundManager;
import com.livermor.hatethisfact.ui.interfaces.MyHatedList;
import com.livermor.hatethisfact.ui.interfaces.Top37Hated;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {

    /*
     **********************************************************************************************/

    //private static final String TAG = MainActivity.class.getSimpleName() + " reports: ";
    static DatabaseSQLiteSingleton db;
    static SharedPreferences prefs;


    //drawer
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawer;
    static NavigationView mNavigationView;
    public static final String VISIBLE_FRAGMENT = "VISIBLE_FRAGMENT";

    CallbackManager mCallbackManager;//fb sdk

    private static Fragment mCurrentFragment;

    private static volatile Activity instance;


    /*
    ***********************************************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        mCallbackManager = CallbackManager.Factory.create();//without it app will crush by cancelling share
        prefs = getSharedPreferences(MySharedPrefs.MAIN, MODE_PRIVATE);
        instance = this;

        /*
        BackendHelper.addFactsToBackend();
        */

        //this creates option menu on samsungs
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //DRAWER and ACTIONBAR SETTINGS
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();
                menuItem.setChecked(true);
                mCurrentFragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.mainCollection:
                        mCurrentFragment = new MainFragment();
                        break;
                    case R.id.topHated37:
                        mCurrentFragment = new Top37Hated();
                        break;
                    case R.id.myHated:
                        mCurrentFragment = new MyHatedList();
                        break;
                }

                if (mCurrentFragment != null) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.container, mCurrentFragment, VISIBLE_FRAGMENT);
                    ft.commit();
                }

                return true;
            }
        });
        mDrawer = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                if (newState == 2) {//state changed
                    SoundManager.playSound(R.raw.menu_sound);
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        // start first main fragment in the beginning
        if (Application.isFirstStart()) {
            mCurrentFragment = new MainFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, mCurrentFragment, VISIBLE_FRAGMENT);
            ft.addToBackStack(null);
            ft.commit();
            Application.setFirstStart(false);
        }

        mNavigationView.getMenu().getItem(0).setChecked(true);
    }//OnCreate

    /*
     * MENU and DRAWER
     ***********************************************************************************************/

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawer.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawer.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem menuItem = menu.findItem(R.id.sound_changer);
        boolean soundOn = prefs.getBoolean(MySharedPrefs.SOUND_MODE, true);
        menuItem.setTitle(R.string.menu_sound_on);
        if (soundOn) menuItem.setTitle(R.string.menu_sound_off);

        MenuItem menuItem2 = menu.findItem(R.id.surprise);
        boolean surpriseOn = prefs.getBoolean(MySharedPrefs.SURPRISE_MODE, true);
        menuItem2.setTitle(R.string.menu_surprise_on);
        if (surpriseOn) menuItem2.setTitle(R.string.menu_surprise_off);

        MenuItem menuItem3 = menu.findItem(R.id.color_change);
        boolean isCrazyColorModeSelected = prefs.getBoolean(MySharedPrefs.COLOR_SCHEME, true);
        menuItem3.setTitle(R.string.menu_color_bright);
        if (isCrazyColorModeSelected) menuItem3.setTitle(R.string.menu_color_light);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (mDrawer.onOptionsItemSelected(item)) {
            return true;
        }

        switch (id) {
            case R.id.surprise:
                changeSurprises();
                SoundManager.playSound();
                invalidateOptionsMenu();//refresh menu
                return true;
            case R.id.share:
                ShareManager.showDialog(getString(R.string.share_title_basic));
                SoundManager.playSound();
                return true;
            case R.id.sound_changer:
                SoundManager.changeSoundOption();
                SoundManager.playSound();
                invalidateOptionsMenu();//refresh menu
                return true;
            case R.id.color_change:
                ColorManager.changeColorOption(prefs);
                SoundManager.playSound();
                invalidateOptionsMenu();//refresh menu


                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment mCurrentFragment = getSupportFragmentManager().findFragmentByTag(VISIBLE_FRAGMENT);

                if (mCurrentFragment instanceof MainFragment) {
                    //separate logic for main fragment, cause it has different structure
                    ft.replace(R.id.container, new MainFragment(), VISIBLE_FRAGMENT);
                    ft.commit();
                    return true;
                }

                ft.detach(mCurrentFragment);
                ft.attach(mCurrentFragment);
                ft.commit();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //cat appears with determined probability
    private void changeSurprises() {
        boolean b = prefs.getBoolean(MySharedPrefs.SURPRISE_MODE, true);
        prefs.edit().putBoolean(MySharedPrefs.SURPRISE_MODE, !b).commit();
        invalidateOptionsMenu();//refresh menu
    }

    public static NavigationView getNavigationView() {
        return mNavigationView;
    }

    /*
    ************************************************************************************************/

    @Override //for fb api
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        db.getInstance();
        PopupManager.rateAndAcquaintance(this);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                MainActivity.this,
                R.style.my_alert_dialog_style
        );

        builder.setTitle(getString(R.string.exit_prevent));

        builder.setPositiveButton(R.string.answer_positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseSQLiteSingleton.setFactToShare(null);
                Application.setFirstStart(true);
                MainActivity.this.finish();
            }
        });

        BackendHelper.getFactsFromServer(50);

        builder.setNegativeButton(
                getString(R.string.answer_negative_emotional),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();
        //super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //for notification purpose
        Intent intent = new Intent(this, NotificationMan.class);
        intent.putExtra(NotificationMan.EXTRA_MESSAGE,
                DatabaseSQLiteSingleton.getFactForNotification());
        startService(intent);
    }

    /*
    Getters
    ***********************************************************************************************/
    public static Activity getInstance() {
        return instance;
    }


}//The end!

