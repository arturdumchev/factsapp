package com.livermor.hatethisfact.ui.puppeteers;


import android.content.SharedPreferences;

import android.os.Bundle;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.ToxicBakery.viewpager.transforms.CubeOutTransformer;
import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.data.BackendHelper;
import com.livermor.hatethisfact.data.DatabaseSQLiteSingleton;
import com.livermor.hatethisfact.data.MySharedPrefs;
import com.livermor.hatethisfact.managers.SoundManager;
import com.livermor.hatethisfact.ui.interfaces.SlideFragment;

import java.util.Random;


public class MainFragment extends Fragment {
    /*
    * *************************************************************************************************************************/
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private int selectedPage;
    static SharedPreferences prefs;
    int factsAmount;

    Handler mHandler;
    int count = 1;
    private final String TAG = MainFragment.class.getSimpleName() + " REPORTS(!)";

    //surprise
    final static private int SURPRISE_PROBABILITY = 35; //from thousand
    private int numberForCat;

    //getting facts from backend
    private int criticalBuffer = 100;
    private int newFacts = 300;


    /*
    * *************************************************************************************************************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.main_fragment, container, false);

        prefs = MySharedPrefs.getmPrefs();

        factsAmount = DatabaseSQLiteSingleton.getInstance().getCursorForMainCollection().getCount();

        mPager = (ViewPager) layout.findViewById(R.id.pager_fragment);
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setPageTransformer(true, new CubeOutTransformer()); //set the animation
        selectedPage = prefs.getInt(MySharedPrefs.SELECTED_PAGE, 0);
        mPager.setCurrentItem(selectedPage); //set the current page


        //When amount of new facts in < then "criticalBuffer",
        // then download +"newFacts" facts
        if (selectedPage + criticalBuffer > factsAmount) {
            BackendHelper.getFactsFromServer(newFacts);
        }

        mPager.beginFakeDrag();

        //if sound is disabled, no need to addOnPage..Listener
        if (!prefs.getBoolean(MySharedPrefs.SOUND_MODE, true)) return layout;
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                if (prefs.getBoolean(MySharedPrefs.SURPRISE_MODE, true)
                        && numberForCat == position) {
                    SoundManager.playRandHorrorSound();
                    BackendHelper.getFactsFromServer(50); //downloading facts from server at this point
                } else {
                    SoundManager.playSound(R.raw.pop_short);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        return layout;
    }
    /**************************************************************************************************************************/
    /**************************************************************************************************************************/

    @Override
    public void onResume() {
        super.onResume();
        selectedPage = prefs.getInt(MySharedPrefs.SELECTED_PAGE, 0);
        mPager.setCurrentItem(selectedPage); //set the current page
    }

    @Override
    public void onPause() {
        super.onPause();



        prefs.edit().putInt(MySharedPrefs.SELECTED_PAGE, mPager.getCurrentItem()).commit();
        DatabaseSQLiteSingleton.closeCursorForMainCollection();
    }


    /*************************************************************************************************************************
     * Adapter
     * Slide fragment items
     **************************************************************************************************************************/
    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            super.finishUpdate(container);

            mHandler = new Handler();
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    if (mPager.isFakeDragging()) {
                        if (count < 12) {
                            count++;
                            mPager.fakeDragBy(-count * count);
                            mHandler.postDelayed(this, 30);
                        } else {
                            mPager.endFakeDrag();
                        }
                    }
                }
            });
        }

        @Override //using current page
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);

            if (object instanceof SlideFragment) {
                SlideFragment fragment = (SlideFragment) object;
                String s = fragment.getTextHelper();//

                if (s == null) {//for cat surprise
                    DatabaseSQLiteSingleton.setFactToShare(getString(R.string.share_cat_text));
                } else {
                    DatabaseSQLiteSingleton.setFactToShare(s);//share CURRENT fact
                }

            }
        }

        @Override
        public Fragment getItem(int position) {
            final Bundle bundle = new Bundle();
            bundle.putInt(SlideFragment.EXTRA_POSITION, position);

            boolean normalState = true;
            numberForCat = -1;
            if (prefs.getBoolean(MySharedPrefs.SURPRISE_MODE, true)
                    && SURPRISE_PROBABILITY > new Random().nextInt(1000)) {
                normalState = false; //state with CAT
                numberForCat = position;
            }
            bundle.putBoolean(SlideFragment.EXTRA_VIEW, normalState);

            final SlideFragment fragment = new SlideFragment();
            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public int getCount() {
            return factsAmount;
        }
    }
}
