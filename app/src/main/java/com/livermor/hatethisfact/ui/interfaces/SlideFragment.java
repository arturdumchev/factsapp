package com.livermor.hatethisfact.ui.interfaces;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.livermor.hatethisfact.R;

import com.livermor.hatethisfact.data.DatabaseSQLiteSingleton;
import com.livermor.hatethisfact.data.ExternalDataBaseHelper;
import com.livermor.hatethisfact.data.BackendHelper;
import com.livermor.hatethisfact.data.MySharedPrefs;
import com.livermor.hatethisfact.managers.ColorManager;
import com.livermor.hatethisfact.managers.SoundManager;

/**
 * Created by arturdumchev on 18.09.15.
 */
public class SlideFragment extends Fragment {

    private static final String TAG = SlideFragment.class.getSimpleName() + "  re—fucking-ports";

    public static final String EXTRA_POSITION = "EXTRA_POSITION";
    public static final String EXTRA_VIEW = "EXTRA_POSITION_2";

    TextView mHateText;
    CheckBox mHateCheckBox;
    private int mPosition;
    private static String mFactToShow;
    boolean mHateCheckBoxIsChecked; //without it being here nothing works well

    DatabaseSQLiteSingleton db;

    String textHelper = "";

    SharedPreferences prefs;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefs = getActivity().getSharedPreferences(MySharedPrefs.MAIN, getActivity().MODE_PRIVATE);
        boolean b = getArguments().getBoolean(EXTRA_VIEW);

        if (b) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.main_slide_page, container, false);
            db.getInstance();

            mPosition = getArguments().getInt(EXTRA_POSITION);
            mHateText = (TextView) rootView.findViewById(R.id.textView);

            //using cursor from siglton to get facts
            db.getCursorForMainCollection().moveToPosition(mPosition);
            mFactToShow = db.getCursorForMainCollection().getString(1);//first row is text of the fact

            mHateText.setText(mFactToShow);
            textHelper = mFactToShow;

            //working with CheckView
            mHateCheckBox = (CheckBox) rootView.findViewById(R.id.hateCheckBox);
            if (db.getCursorForMainCollection().getInt(2) == 1) {
                mHateCheckBox.setChecked(true);
            } else {
                mHateCheckBox.setChecked(false);
            }
            mHateCheckBoxIsChecked = mHateCheckBox.isChecked();
            mHateCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SoundManager.playSound(R.raw.hate_sound);

                    //needed to save hatred to backend
                    if (mHateCheckBoxIsChecked) {
                        changeHateAttributeInSQLite(false);
                    } else {
                        changeHateAttributeInSQLite(true);
                    }
                    mHateCheckBoxIsChecked = mHateCheckBox.isChecked();
                }
            });

            int color = ColorManager.getColor();
            rootView.setBackgroundColor(color);

            if (ColorManager.isDarkColor(color)){
                //it will be forgotten cause ViewPager remember only 3 layouts in a moment
                mHateText.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_for_dark));
                mHateCheckBox.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_for_dark));
                mHateCheckBox.setButtonDrawable(R.drawable.check_box_white);
            }

            return rootView; //return the slide view

        } else {

            ViewGroup rootView2 = (ViewGroup) inflater.inflate(R.layout.main_slide_page_alternative, container, false);
            CheckBox mHateCatCheckBox = (CheckBox) rootView2.findViewById(R.id.hateCheckBox2);
            mHateCatCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SoundManager.playSound(R.raw.cat_hated_sound);
                }
            });

            textHelper = null;
            return rootView2;
        }
    }

    private void changeHateAttributeInSQLite(Boolean b) {
        BackendHelper.saveHatred(textHelper, b);

        SQLiteDatabase db = this.db.getDb();
        ContentValues factValues = new ContentValues();
        factValues.put(ExternalDataBaseHelper.IS_HATED, b);
        db.update(ExternalDataBaseHelper.TABLE_NAME,
                factValues,
                "FACT = ?", // where Fact == current fact
                new String[]{textHelper});//don't know why, but I cant just set it with mFactToShow(), cause it gona be previous text somehow
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        DatabaseSQLiteSingleton.setFactForNotification(mFactToShow);
    }

    public String getTextHelper() {
        return textHelper;
    }
}
