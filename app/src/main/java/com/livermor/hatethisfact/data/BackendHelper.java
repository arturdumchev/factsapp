package com.livermor.hatethisfact.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


import com.livermor.hatethisfact.Application;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by arturdumchev on 20.09.15.
 */
public class BackendHelper {
    private static final String TAG = BackendHelper.class.getSimpleName();

    //keys
    private static final String APPLICATION_ID = "viOttYuow8IsrNdcvaSRB6jXDcKtNX8x69tp7Dqk";
    private static final String CLIENT_KEY = "Uoo7lHX9KzbClT0MgGAgwVwAa8vimYXZHx9HMt33";

    // Table names
    public static String KEY_FACTS_TABLE = "facts";

    // Field names
    public static String KEY_FACT = "fact";
    public static String KEY_REPUTATION = "reputation";
    public static String KEY_FACT_NUMBER = "number";

    static String raw = ""; //for new facts

    //functions
    public static void initialization(Context context) {

        Parse.enableLocalDatastore(context);
        Parse.initialize(context, APPLICATION_ID, CLIENT_KEY);
    }

    public static void getFactsFromServer(int amount) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(KEY_FACTS_TABLE);
        query.whereGreaterThan(KEY_FACT_NUMBER, DatabaseSQLiteSingleton.getCursorForMainCollection().getCount());
        query.setLimit(amount);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {//success

                    for (ParseObject fact : list) {

                        ExternalDataBaseHelper.insertFacts(
                                DatabaseSQLiteSingleton.getDb(),
                                fact.getString(BackendHelper.KEY_FACT),
                                0
                        );
                        // Log.w(TAG, String.valueOf(fact.getString(BackendHelper.KEY_FACT)));
                    }
                    //   Log.w(TAG, "Attempt to download 300 facts!");
                } else {//fail
                    // Log.w("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private static boolean isNetworkAvailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }
        return isAvailable;
    }

    public static void updateDatabase(Context context) {
        //update database in backend
        if (!isNetworkAvailable(context)) {
            return;
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery(KEY_FACTS_TABLE);
        query.orderByDescending(KEY_REPUTATION);
        query.setLimit(37);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> list, ParseException e) {

                for (ParseObject hatedFact : list) {

                    String tempFact = hatedFact.getString(BackendHelper.KEY_FACT);
                    int tempRep = hatedFact.getInt(BackendHelper.KEY_REPUTATION);
                    changeReputationAttributeInSQLite(tempFact, tempRep);
                }
            }
        });
    }

    private static void changeReputationAttributeInSQLite(String fact, int rep) {
        SQLiteDatabase db = DatabaseSQLiteSingleton.getDb();

        ContentValues factValues = new ContentValues();
        factValues.put(ExternalDataBaseHelper.REP_HATED, rep);

        if (db == null) {
            return;
        }
        db.update(ExternalDataBaseHelper.TABLE_NAME,
                factValues,
                "FACT = ?",
                new String[]{fact});
    }

    public static void saveHatred(String fact, final boolean b) {//decrement fact rep if user taps  "Hate it!"
        ParseQuery<ParseObject> query = ParseQuery.getQuery(BackendHelper.KEY_FACTS_TABLE);
        query.whereEqualTo(BackendHelper.KEY_FACT, fact);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    for (ParseObject fact : list) {
                        int increment = fact.getInt(BackendHelper.KEY_REPUTATION);
                        fact.put(BackendHelper.KEY_REPUTATION, (b) ? increment + 1 : increment - 1);
                        fact.saveEventually();
                    }
                } else {
                    //   Log.w("score", "Error: " + e.getMessage());
                }
            }
        });
    }


    //helper for adding facts from assets to backend
    public static void addFactsToBackend() {

        AssetManager am = Application.get().getAssets();
        try {
            InputStream input = am.open("raw.txt");
            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            // byte buffer into a string
            raw = new String(buffer);


        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] array = raw.split("\n");
        Log.w(TAG, String.valueOf(array.length));

        for (int number = 0; number < array.length; number++) {

            ParseObject gameScore = new ParseObject(KEY_FACTS_TABLE);
            gameScore.put(KEY_FACT, array[number]);
            gameScore.put(KEY_REPUTATION, 1);
            gameScore.put(KEY_FACT_NUMBER, number + 1023);
            try {
                gameScore.save();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}


