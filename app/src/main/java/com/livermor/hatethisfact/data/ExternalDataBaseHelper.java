package com.livermor.hatethisfact.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 * Created by arturdumchev on 21.09.15.
 */
public class ExternalDataBaseHelper extends SQLiteOpenHelper{

    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.livermor.hatethisfact/databases/";

    private SQLiteDatabase myDataBase;
    private final Context myContext;

    public static String DB_NAME = "hatefacts";
    public static final int DB_VERSION = 2; //upgrade will only works, if I change it


    public static final String TABLE_NAME = "FACTS";
    //Table fields
    public static final String FACT_FIELD = "FACT";
    public static final String IS_HATED = "HATED";
    public static final String REP_HATED = "REP";


    public ExternalDataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.myContext = context;
    }
    /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if(dbExist){
            //do nothing - database already exist
        }else{
            //By calling this method and empty database will be created into the default system path
            //of application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {throw new Error("Error copying database");}
        }
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;
        try{
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }catch(SQLiteException e){
        //database does't exist yet.
        }

        if(checkDB != null){checkDB.close();}

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {
        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE FACTS ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "FACT TEXT, "
                + "HATED INTEGER"
                + "REP);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    //my function for insertion
    public static void insertFacts(SQLiteDatabase db, String fact, int isHated) {
        //the way we insert data in db
        ContentValues values = new ContentValues();
        values.put(FACT_FIELD, fact);
        values.put(IS_HATED, isHated);
        values.put(REP_HATED, 1);//new facts comes with 1 rep.
        db.insert(TABLE_NAME, null, values);
    }
}
//instruction
//http://blog.reigndesign.com/blog/using-your-own-sqlite-database-in-android-applications/
