package com.livermor.hatethisfact.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.livermor.hatethisfact.Application;

/**
 * Created by arturdumchev on 14.10.15.
 */
public class MySharedPrefs {

    //name
    public static final String MAIN = "com.livermor.hatethisfact";

    private static Context mContext = Application.get();
    private static SharedPreferences mPrefs = mContext.getSharedPreferences(MAIN, mContext.MODE_PRIVATE);

    //first visit
    public static final String FIRST_RUN_TOP = "firstrunTOP";
    public static final String FIRST_RUN = "firstrun";

    //modes
    public static final String SURPRISE_MODE = "surpriseMode";
    public static final String COLOR_SCHEME = "colorScheme";
    public static final String SOUND_MODE = "soundMode";
    public static final String SELECTED_PAGE = "selectedPage";

    //rate this app
    public static final String RATING_TIME = "ratingTime";

    public static SharedPreferences getmPrefs() {
        return mPrefs;
    }
}

