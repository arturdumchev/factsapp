package com.livermor.hatethisfact.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.livermor.hatethisfact.Application;
import java.io.IOException;


/**
 * Created by arturdumchev on 25.09.15.
 */
public class DatabaseSQLiteSingleton {

    private static Context mContext;
    private static Cursor mCursor;
    private static Cursor mCursor2;
    private static Cursor mCursor3;
    private static SQLiteDatabase db;
    private static String mFactToShare = null;
    private static String mFactForNotification = null;

    private volatile static DatabaseSQLiteSingleton instance;


    private DatabaseSQLiteSingleton() {

        mContext = Application.get();

        ExternalDataBaseHelper externalDataBaseHelper = new ExternalDataBaseHelper(mContext);
        try {
            externalDataBaseHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        db = externalDataBaseHelper.getWritableDatabase();
    }


    //PUBLIC METHODS
    public static DatabaseSQLiteSingleton getInstance() {

        if (instance == null) {
            synchronized (DatabaseSQLiteSingleton.class) {
                if (instance == null) {
                    instance = new DatabaseSQLiteSingleton();
                }
            }
        }
        return instance;
    }

    //open cursor if it is closed or never existed
    public static Cursor getCursorForMainCollection() {

        if (mCursor == null) {
            mCursor = db.query(
                    ExternalDataBaseHelper.TABLE_NAME,
                    new String[]{"_id", ExternalDataBaseHelper.FACT_FIELD, ExternalDataBaseHelper.IS_HATED},
                    null,
                    null, null, null, null
            );
        }

        return mCursor;
    }

    public static Cursor getCursorForMyHatedList() {

        if (mCursor2 == null) {
            mCursor2 = db.query(
                    ExternalDataBaseHelper.TABLE_NAME,
                    new String[]{"_id", ExternalDataBaseHelper.FACT_FIELD, ExternalDataBaseHelper.IS_HATED},
                    "HATED = 1",
                    null, null, null, null
            );
        }

        return mCursor2;
    }

    public static Cursor getCursorForTOP37Hated() {

        if (mCursor3 == null) {
            mCursor3 = db.query(
                    ExternalDataBaseHelper.TABLE_NAME,
                    new String[]{"_id", ExternalDataBaseHelper.FACT_FIELD, ExternalDataBaseHelper.REP_HATED},
                    null,
                    null,
                    null,
                    null,
                    ExternalDataBaseHelper.REP_HATED + " DESC"
            );
        }

        return mCursor3;
    }


    public static SQLiteDatabase getDb() {
        return db;
    }

    public static void closeCursorForMainCollection() {
        if (mCursor != null) {
            mCursor.close();
            mCursor = null;
        }

    }

    public static void closeCursorForMyHatedList() {
        if (mCursor2 != null) {
            mCursor2.close();
            mCursor2 = null;
        }

    }

    public static void closeCursorForTOP37Hated() {
        if (mCursor3 != null) {
            mCursor3.close();
            mCursor3 = null;
        }

    }

    public static void closeDatabase() {
        if (db != null) {
            db.close();
            db = null;
        }
    }

    public static String getFactToShare() {
        return mFactToShare;
    }

    public static void setFactToShare(String mFactToShare) {
        DatabaseSQLiteSingleton.mFactToShare = mFactToShare;
    }

    public static String getFactForNotification() {
        return mFactForNotification;
    }

    public static void setFactForNotification(String mFactForNotification) {
        DatabaseSQLiteSingleton.mFactForNotification = mFactForNotification;
    }
}
