package com.livermor.hatethisfact.managers;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.data.DatabaseSQLiteSingleton;
import com.livermor.hatethisfact.ui.puppeteers.MainActivity;

/**
 * Created by arturdumchev on 06.10.15.
 */
public class ShareManager {

    private static String mFactToShare;
    private static ShareLinkContent mContent;
    private volatile static Activity mActivity = MainActivity.getInstance();


    private static String mySiteName = mActivity.getString(R.string.link_site_my);

    public static void showDialog(String title, String shareText) {

        mFactToShare = shareText;

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);

        if (title != null) builder.setTitle(title);

        builder.setPositiveButton(mActivity.getString(R.string.share_choice_facebook), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mContent = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(mySiteName))
                        .setContentTitle(mActivity.getString(R.string.app_name))
                        .setContentDescription(mFactToShare)
                        .build();

                ShareDialog.show(mActivity, mContent);
            }
        });
        builder.setNegativeButton(mActivity.getString(R.string.share_choice_other), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, mFactToShare + "\n" + mySiteName);
                Intent chosenIntent = Intent.createChooser(intent, null);
                mActivity.startActivity(chosenIntent);
            }
        });
        builder.show();
    }

    public static void showDialog(String title) {

        mFactToShare = DatabaseSQLiteSingleton.getFactToShare();

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);

        if (title != null) builder.setTitle(title);

        builder.setPositiveButton(mActivity.getString(R.string.share_choice_facebook), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mContent = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(mySiteName))
                        .setContentTitle(mActivity.getString(R.string.app_name))
                        .setContentDescription(mFactToShare)
                        .build();

                ShareDialog.show(mActivity, mContent);
            }
        });
        builder.setNegativeButton(R.string.share_choice_other, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, mFactToShare + "\n" + mySiteName);
                Intent chosenIntent = Intent.createChooser(intent, null);
                mActivity.startActivity(chosenIntent);
            }
        });
        builder.show();
    }
}
