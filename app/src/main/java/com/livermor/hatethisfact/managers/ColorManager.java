package com.livermor.hatethisfact.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;

import com.livermor.hatethisfact.Application;
import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.data.MySharedPrefs;

import java.util.Random;

/**
 * Created by arturdumchev on 26.09.15.
 */
public class ColorManager {

    private static Context mContext = Application.get();
    private static SharedPreferences prefs = mContext.getSharedPreferences(MySharedPrefs.MAIN, mContext.MODE_PRIVATE);
    private static boolean middleColor;



    public static int getBackgroundColor() {
        if (prefs.getBoolean(MySharedPrefs.COLOR_SCHEME, true)) {
            return ContextCompat.getColor(mContext, R.color.crazy_background_color);
        } else {
            return ContextCompat.getColor(mContext, R.color.light_background_color);
        }
    }

    public static int getColor() {

        Random rnd = new Random();

        if (prefs.getBoolean(MySharedPrefs.COLOR_SCHEME, true)) {
            int[] crazyColors = mContext.getResources().getIntArray(R.array.crazy_mode_colors);
            return crazyColors[rnd.nextInt(crazyColors.length)];

        } else {

            if (middleColor) {
                int[] easyLightColors = mContext.getResources().getIntArray(R.array.easy_light_colors);
                middleColor = false;
                return easyLightColors[rnd.nextInt(easyLightColors.length)];

            } else {
                int[] easyDarkColors = mContext.getResources().getIntArray(R.array.easy_dark_colors);
                middleColor = true;
                return easyDarkColors[rnd.nextInt(easyDarkColors.length)];
            }
        }
    }

    public static boolean isDarkColor(int color) {

        int[] colors = mContext.getResources().getIntArray(R.array.dark_colors);

        for (int c : colors) {
            if(c == color) return true;
        }

        return false;
    }

    public static void changeColorOption(SharedPreferences prefs) {
        boolean b = prefs.getBoolean(MySharedPrefs.COLOR_SCHEME, true);
        prefs.edit().putBoolean(MySharedPrefs.COLOR_SCHEME, !b).commit();
    }


}
