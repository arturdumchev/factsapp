package com.livermor.hatethisfact.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;

import com.livermor.hatethisfact.Application;
import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.data.MySharedPrefs;

import java.util.Random;

/**
 * Created by arturdumchev on 06.10.15.
 */
public class SoundManager {

    private static Context mContext = Application.get();
    private static SharedPreferences mPrefs = MySharedPrefs.getmPrefs();

    public static void changeSoundOption() {
        boolean b = mPrefs.getBoolean(MySharedPrefs.SOUND_MODE, true);
        mPrefs.edit().putBoolean(MySharedPrefs.SOUND_MODE, !b).commit();
    }

    public static void playSound(int sound) {

        if (mPrefs.getBoolean(MySharedPrefs.SOUND_MODE, true)) {
            MediaPlayer mp = MediaPlayer.create(mContext, sound);
            mp.start();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }
    }

    public static void playSound() {

        if (!mPrefs.getBoolean(MySharedPrefs.SOUND_MODE, true)) {
            return;
        }
        MediaPlayer mp = MediaPlayer.create(mContext, R.raw.menu_sound);
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
    }

    public static void playRandHorrorSound() {

        MediaPlayer mp;
        Random random = new Random();
        int i2 = random.nextInt(2);
        if (i2 == 1) {
            mp = MediaPlayer.create(mContext, R.raw.horror1);
        } else {
            mp = MediaPlayer.create(mContext, R.raw.horror2);
        }
        mp.start();

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
    }

}
