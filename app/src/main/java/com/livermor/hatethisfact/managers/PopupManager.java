package com.livermor.hatethisfact.managers;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;

import com.livermor.hatethisfact.R;
import com.livermor.hatethisfact.data.MySharedPrefs;
import com.livermor.hatethisfact.ui.puppeteers.MainActivity;

/**
 * Created by arturdumchev on 20.10.15.
 */
public class PopupManager {
    private static final String TAG = PopupManager.class.getSimpleName();

    private static long timeToAskForRate = 1000 * 60 * 60 * 24;//ask after one day
    private static long rateIfDontLike = 1000 * 60 * 60 * 24 * 10;//if don't like the app
    private static long rateIfDontAgree = 1000 * 60 * 60 * 24 * 20;//if don't agree to rate

    private static long firstIntrodactionQuoteTime = 1000 * 20; // "no one can take away your right to hate things"
    private static long never = 1000 * 60 * 60 * 24 * 999l; // if already vote


    //    private static Activity mActivity = MainActivity.get();
    private static Activity mActivity;
    private static SharedPreferences mPrefs;

    /************************************************************************************************************************
     * This will cares about all the rate stuff
     ***********************************************************************************************************************/
    public static void rateAndAcquaintance(Activity a) {
        mPrefs = MySharedPrefs.getmPrefs();
        mActivity = a;
        firstStartPopup();
        ratePopup();
    }

    /************************************************************************************************************************
     * First popup for Top37hated fragment
     ************************************************************************************************************************/
    public static void firstStart37() {

        mActivity = MainActivity.getInstance();
        mPrefs = MySharedPrefs.getmPrefs();


        if (mPrefs.getBoolean(MySharedPrefs.FIRST_RUN_TOP, true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
            builder.setMessage(R.string.popup_first_top37)
                    .setPositiveButton(android.R.string.ok, null);
            AlertDialog dialog = builder.create();
            dialog.show();

            mPrefs.edit().putBoolean(MySharedPrefs.FIRST_RUN_TOP, false).apply();
        } else {
            return;
        }
    }

    /************************************************************************************************************************
     * Rate popups
     * all logic is here
     ***********************************************************************************************************************/
    private static void ratePopup() {

        //false first time
        if (mPrefs.getLong(MySharedPrefs.RATING_TIME, System.currentTimeMillis() + timeToAskForRate)
                >=
                System.currentTimeMillis()) {

            // Log.w(TAG, " don't show now!");

            return;
        }

        if (mActivity == null) return;


        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
        builder.setTitle(mActivity.getString(R.string.rate_title_first));

        builder.setPositiveButton(mActivity.getString(R.string.answer_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                AlertDialog.Builder builder
                        = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
                builder.setTitle(mActivity.getString(R.string.rate_title_second));

                builder.setPositiveButton(mActivity.getString(R.string.rate_answer_agree), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //never ask again
                        mPrefs.edit().putLong(MySharedPrefs.RATING_TIME, System.currentTimeMillis() + never).apply();
                        String packageName = mActivity.getPackageName();

                        Uri uri = Uri.parse(mActivity.getString(R.string.link_google_market) + packageName);
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        // To count with Play market backstack, After pressing back button,
                        // to taken back to our application, we need to add following flags to intent.
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            mActivity.startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(mActivity.getString(R.string.link_google_play) + packageName)));
                        }

                    }
                });
                //question was: Do you want to rate?
                builder.setNegativeButton(mActivity.getString(R.string.answer_negative), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ask again after 10 days
                        mPrefs.edit().putLong(MySharedPrefs.RATING_TIME, System.currentTimeMillis() + rateIfDontAgree).apply();
                    }
                });
                builder.show();
            }
        });
        //question was: Do you like this app?
        builder.setNegativeButton(R.string.answer_negative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //ask again after 20 days
                mPrefs.edit().putLong(MySharedPrefs.RATING_TIME, System.currentTimeMillis() + rateIfDontLike).apply();
            }
        });
        builder.show();

    }

    /************************************************************************************************************************
     * First popup
     *************************************************************************************************************************/
    private static void firstStartPopup() {

        // if first run
        if (mPrefs.getBoolean(MySharedPrefs.FIRST_RUN, true)) {

            if (mActivity == null) return;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
                    builder.setMessage(R.string.popup_first_basic)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();

                    if (mActivity == null || mActivity.isFinishing()) {
                        return;
                    }
                    dialog.show();
                }
            }, firstIntrodactionQuoteTime);

            if (mActivity == null || mActivity.isFinishing()) {
                return;
            }

            //set rating time to "timeToAskForRate" from now
            mPrefs.edit().putLong(
                    MySharedPrefs.RATING_TIME,
                    System.currentTimeMillis() + timeToAskForRate)
                    .apply();

            //never first run again
            mPrefs.edit().putBoolean(MySharedPrefs.FIRST_RUN, false).apply();
        }
    }


}
